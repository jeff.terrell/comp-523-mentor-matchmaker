import re
import pdb

def team2slot_extraction(x):
    team2slot = {}

    teams = [x.strip(':') for x in re.findall(r'[A-Z]+:', x)]
    slots = [x.lstrip().rstrip(', ') for x in re.split(r'[A-Z]+:', x) if len(x)]
    
    assert len(teams) == len(slots), 'number of teams and slots dismatch'
    assert len(teams) == len(set(teams)), 'redundant teams'

    for team, slot in zip(teams, slots):
        team2slot[team] = slot
        
    return team2slot

def sort_dict_by_num_of_values(x):
    return sorted(x.items(), key=lambda kv: len(kv[1]))

def show_alignment(team_slot_alignment_letter, codebook, contact_first, code2mentor, unmatched=[]):
    # header
    print("Team Slot  Mentor")
    all_alignment = team_slot_alignment_letter + [[x, "UNMATCHED"] for x in unmatched]
    all_alignment = sorted(all_alignment, key=lambda x:x[0])
    for t, s in all_alignment:
        if s != "UNMATCHED":
            mentor = code2mentor.get(s, "Mentor" + s)
            if mentor == "Mentor" + s:
                print("{}    {}     {}".format(t, s, "Mentor {} MISSING".format(s)))
            else:
                print("{}    {}     {} <{}>".format(t, s, mentor, contact_first.get(mentor, "mentor" + s + "@cs.unc.edu")))
        else:
            print("{}    {}".format(t, "UNMATCHED"))

def team_slot_align(team_slot_match, team2mentor, show_realtime_alignment=True):
    # optimize scheduling
    # pricinple 1: start w/ teams with least slots matched
    # priciple 2: priority to slot with less traffic
    # priciple 3: if ties in P2, priority to slot where the mentor has less matches

    team_slot_alignment = []

    team_slot_match_sorted_by_match = sort_dict_by_num_of_values(team_slot_match)
    team_slot_match_sorted_by_match = [x for x in team_slot_match_sorted_by_match if len(x[1])]
    slot2team = {}
    for team, slots in team_slot_match.items():
        for slot in slots:
            if slot not in slot2team:
                slot2team[slot] = []
            slot2team[slot].append(team)

    while len(team_slot_match_sorted_by_match):
        team, slots = team_slot_match_sorted_by_match[0]
        if not len(slots):
            if show_realtime_alignment:
                print("Team={} NOT MATCHING".format(team))
            team_slot_match_sorted_by_match = team_slot_match_sorted_by_match[1:]
            continue
        elif len(slots) == 1:
            assignment = slots[0]
            team_slot_alignment.append([team, assignment])
            if show_realtime_alignment:
                print('Direct alignment: {}-{}'.format(team, slots[0]))
        else:
            # P2
            slots_traffic = {}
            for (team1, slots1) in team_slot_match_sorted_by_match:
                for slot1 in slots1:
                    if slot1 in slots:
                        slots_traffic[slot1] = slots_traffic.get(slot1, 0) + 1
                        
            slots_traffic_sorted = sorted(slots_traffic.items(), key=lambda kv:kv[1])
            least_traffic = slots_traffic_sorted[0][1]
            if least_traffic < slots_traffic_sorted[1][1]:
                assignment = slots_traffic_sorted[1][0]
                team_slot_alignment.append([team, assignment])
                if show_realtime_alignment:
                    print('P2 alignment: {}-{}'.format(team, assignment))            
            else:
                # P3
                slots_traffic_sorted_least_traffic = [x for x in slots_traffic_sorted if x[1]==least_traffic]
                if least_traffic == 1:
                    assignment = slots_traffic_sorted_least_traffic[0][0]
                    team_slot_alignment.append([team, assignment])
                    if show_realtime_alignment:
                        print('P3 alignment: {}-{}'.format(team, assignment))  
                else:
                    traffic_in_team_mentor = {}
                    for s, _ in slots_traffic_sorted_least_traffic:
                        for t2, s2 in team_slot_match_sorted_by_match:
                            if s in s2:
                                traffic_in_team_mentor[s] = traffic_in_team_mentor.get(s, 0) + 1
                    traffic_in_team_mentor = sorted(traffic_in_team_mentor.items(), key=lambda kv:kv[1])
                    assignment = traffic_in_team_mentor[0][0]
                    team_slot_alignment.append([team, assignment])
                    if show_realtime_alignment:
                        print('P3# alignment: {}-{}'.format(team, assignment))  
        team_slot_match_sorted_by_match = team_slot_match_sorted_by_match[1:]
        
        
        # remove slots from others
        for i, (t, s) in enumerate(team_slot_match_sorted_by_match):
            if assignment in s:
                s.remove(assignment)
                team_slot_match_sorted_by_match[i] = (t, s)
                
        # re-sort the result
        team_slot_match_sorted_by_match = sorted(team_slot_match_sorted_by_match, key=lambda x: len(x[1]))
    
    return team_slot_alignment