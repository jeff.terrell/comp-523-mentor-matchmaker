import os
import json
import csv
import pandas as pd
import numpy as np
from util import *
import argparse
import configparser
import datetime


def main(config_file):
    config = configparser.ConfigParser()
    config.read(config_file)
    # print(config.sections())
    data = config['DATA']
    filters = config['FILTER']
    infos = config['INFO']
    paths = config['PATH']

    mentor_availability_file = data['mentor_availability']
    team_availability_file = data['team_availability']
    team_info_file = data['team_info']

    start_date = filters['start_date'] if 'start_date' in filters else '2019-01-01'
    start_date = np.datetime64(start_date)
    show_team_slot_match = infos.getboolean('show_team_slot_match')
    show_no_slot_match_teams = infos.getboolean('show_no_slot_match_teams')
    show_realtime_alignment = infos.getboolean('show_realtime_alignment')
    save_visualization = infos.getboolean('show_visual_alignment')
    visualization_path = paths['visual_alignment']
    result_save_path = paths['alignment_path']

    if save_visualization and not os.path.exists(visualization_path):
        os.mkdir(visualization_path)

    if os.path.exists(mentor_availability_file):
        mentor_availability = pd.read_csv(mentor_availability_file)
        mentor_availability_columns = mentor_availability.columns
        mentor_availability_header = ["Timestamp", "MentorLetter", "MentorFullName", "MentorEmailAddress", "MentorAvailability"]
        missed_header = [x for x in mentor_availability_header if x not in mentor_availability_columns]
        if len(missed_header):
            # tolearate missing timestamp
            if len(missed_header) == 1 and missed_header[0] == "Timestamp":
                pass
            else:
                print("HeaderMissing in mentor availability: {}".format(' '.join(missed_header)))
                if "Timestamp" in missed_header:
                    print("Timestamp can be ignored.")
                print('\n')
                return
    else:
        print("FileMissing: No mentor availability file")
        return

    if os.path.exists(team_availability_file):
        team_availability = pd.read_csv(team_availability_file)
        team_availability_columns = team_availability.columns
        team_availability_header = ["Timestamp", "TeamLetter", "AvailableMentorSlots"]
        missed_header = [x for x in team_availability_header if x not in team_availability_columns]
        if len(missed_header):
            print("HeaderMissing in mentor availability: {}".format(' '.join(missed_header)))
            print('\n')
            return
    else:
        print("FileMissing: No team availability file")
        return


    if os.path.exists(team_info_file):
        team_info = pd.read_csv(team_info_file)
        team_info_columns = team_info.columns
        team_info_header = ["StudentEmail", "TeamLetter"]
        missed_header = [x for x in team_info_header if x not in team_info_columns]
        if len(missed_header):
            print("HeaderMissing in mentor availability: {}".format(' '.join(missed_header)))
            print('\n')
            return
    else:
        print("FileMissing: No team info file to get all team information")
        return
        
    dates = [datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f') for x in team_availability['Timestamp'].values]
    team_availability['Timestamp'] = dates
    team_availability = team_availability[team_availability.Timestamp >= start_date]

    # send out err in when a column is missing
    mentors = list(mentor_availability['MentorFullName'].values)
    mentor_codes = list(mentor_availability['MentorLetter'].values)
    mentor_emails = list(mentor_availability['MentorEmailAddress'].values)
    mentor_slots = list(mentor_availability['MentorAvailability'].values)

    time_slots_teams = [list(team2slot_extraction(x).keys()) for x in team_availability['AvailableMentorSlots'].values]
    teams_members = team_availability['TeamLetter'].values

    # key: slot code, value: time slot
    slot_codebook = {}

    # Update: loaded by config file (hardcode)
    # key: team code, value: mentor name. e.g. Q: Jeff
    # key: team code, value: available slot code for the mentor. e.g. L: ['E', 'H', 'I', 'L']
    code2mentor = {}
    mentor2team = {}
    mentor2contact = {}
    for mentor, mentor_code, mentor_email, mentor_slot in zip(mentors, mentor_codes, mentor_emails, mentor_slots):
        code2mentor[mentor_code] = mentor
        mentor2contact[mentor] = mentor_email
        slot_codebook[mentor_code] = mentor_slot

    team2slot = {}
    all_time_slots = []
    all_time_slots_team_availability = []
    for team, slot in zip(teams_members, time_slots_teams):
        # slot_info = team2slot_extraction(slot)
        # use the most recent submission
        if type(slot) == str:
            slot = eval(slot)
        team2slot[team] = slot
        all_time_slots_team_availability += slot
        for s in slot:
            if s not in all_time_slots:
                all_time_slots.append(s)

    all_time_slots_team_availability = list(set(all_time_slots_team_availability))

    team2index = dict([(team, idx) for idx, team in enumerate(sorted(list(set(teams_members))))])
    slot2index = dict([(slot, idx) for idx, slot in enumerate(sorted(all_time_slots))])
    slot2index['TBD'] = len(slot2index)

    # Missing student emails
    teams_w_contact = list(set(team_info.TeamLetter.values))
    teams_missing_contact_in_poll = [x for x in teams_members if x not in teams_w_contact]
    teams_missing_contact_in_team_availability = [x for x in teams_w_contact if x not in teams_members]
    if len(teams_missing_contact_in_poll):
        print("Contact for team {} is missing.".format(" ".join(teams_missing_contact_in_poll)))
        return

    # Missing team in the poll
    if len(teams_missing_contact_in_team_availability):
        print("Team {} need to fill the poll for their availabilities.".format(" ".join(teams_missing_contact_in_team_availability)))
        return 

    # Missing mentor files
    mentors_in_poll_not_in_mentor_availability = [x for x in all_time_slots_team_availability if x not in all_time_slots]
    if len(mentors_in_poll_not_in_mentor_availability):
        print("Mentors {} doesn't provide availability in mentor availability file.".format(" ".join(mentors_in_poll_not_in_mentor_availability)))
        return

    # find matching
    slot_match_matrix = np.zeros((len(team2index), len(slot2index)))
    team_slot_match = {}
    team_no_match = []

    for team in teams_members:
        team_slots = team2slot[team]
        team_slot_match[team] = team_slots
        for slot in team_slots:
            slot_match_matrix[team2index[team], slot2index[slot]] += 1

    for team in team_no_match:
        slot_match_matrix[team2index[team], slot2index['TBD']] += 1

    team_slot_alignment = team_slot_align(team_slot_match, code2mentor, show_realtime_alignment)

    print("======= Alignment ===========")
    team_matched = [x[0] for x in team_slot_alignment]
    team_unmatched = [x for x in team2index.keys() if x not in team_matched]
    show_alignment(team_slot_alignment, slot_codebook, mentor2contact, code2mentor, team_unmatched)

    print("\n")
    team_alignment_info = {}
    for team, slot_letter in team_slot_alignment:
        if slot_letter in code2mentor:
            mentor_email = mentor2contact.get(code2mentor[slot_letter], "MentorEmailMissing")
            student_emails = list(team_info[team_info.TeamLetter==team]["StudentEmail"].values)
            all_emails = [mentor_email] + student_emails
            team_alignment_info[team] = {
                'time': slot_codebook[slot_letter].replace('@', ' at '),
                'teamLetter': team,
                'mentorFullname': code2mentor[slot_letter],
                'mentorAvailability': slot_codebook[slot_letter],
                'mentorEmail': mentor_email,
                'studentEmails': ','.join(student_emails),
            }
        else:
            print("Mentor {} doesn't exist.".format(slot_letter))

    csv_columns = ['teamLetter', 'time', 'mentorFullname','mentorAvailability','mentorEmail', 'studentEmails']

    teams_info = []
    for team, team_info in team_alignment_info.items():
        teams_info.append(team_info)

    try:
        with open(result_save_path, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            writer.writerows(teams_info) 
    except IOError:
        print("I/O error")

    print("=============================")

    print("Result save at {}".format(result_save_path))

    print("==========  END  ============")

    if save_visualization:
        from visual_matching import visual_alignment
        visual_alignment(team2index, slot2index, code2mentor, slot_codebook, slot_match_matrix, team_slot_match, team_slot_alignment, save_path=visualization_path)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str, default="config/example.ini", help='path to the config file')
    args = parser.parse_args()
    main(args.config)

