# COMP 523 Mentor Matchmaking Program

## Context

Mentors need to be matched with teams. Given information about mentor and team
availability, this program performs the matching. One notable output is the
information necessary to send all the notification emails with the relevant
info merged in.

### Inputs

In the abstract, we need 3 types of information:

1. Information about mentors, including their names, email addresses, and when
   they can meet.
2. For each team, the team letter and the list of which mentors they have
   overlapping availability with.
3. For each team, the list of email addresses associated with that team.

## Operation

### 0. One-time setup

You need to install some python packages the program depends on before running
the program.

    pip install -r requirement.txt

### 1. Configuration

There are several types of configuration.

Email sending is controlled by `mailmerge.server.conf`. More information about
that is below.

The template of the email to be sent to each team and mentor combination is in
`mailmerge.template.txt`.

The rest of the program is controlled by a configuration file. You can find an
example configuration file in `config/example.ini`. Here are the settings you
can control from this file, broken up into sections:

**data**

- `mentor_availability`: where the mentor availability file (Input #1 above)
  can be found. This must be a TSV file with the following header row:
  `MentorLetter	MentorFullName	MentorEmailAddress	MentorAvailability`.
- `team_availability`: where the team availability file (Input #2 above) can be
  found. This must be a TSV file with the following header row:
  `Timestamp	TeamLetter	AvailableMentorSlots`.
- `team_info`: where the team info file (Input #3 above) can be found. This
  must be a TSV file with the following header row:
  `StudentEmail	TeamLetter`.

Examples of these files are available in the `data/` subdirectory.

**path**

- `visual_alignment`: path to save a visualization of the alignment to
- `alignment_path`: path of the result saved (do not change)

**filter**

- `start_date`: the earliest valid date to be used for matchmaking

**info**

(I'm not sure what these mean.)

- `show_team_slot_match`: False
- `show_no_slot_match_teams`: False
- `show_realtime_alignment`: False
- `show_visual_alignment`: False

### 2. Making matches

With a valid configuration file, itself referring to valid input files, say:

    python main.py --config path/to/config.ini

### 3. Checking emails (optional)

One of the outputs of Step 2 is a `mailmerge.database.csv` file containing the
data for each result set (one per combination of team and mentor).

The goal of a "mail merge" process is to merge that data into a template to
send a customized email for each result. Note that the template is stored at
`mailmerge.template.txt`.

To preview the emails that will be sent, but not actually send them, say:

    mailmerge --no-limit > mailmerge.out.txt

The `mailmerge` program is provided by the eponymous Python package. By
default, the `mailmerge` program stops after 1 email, which is why we specify
`--no-limit` here.

### 4. Send emails

To actually for-real send the emails, with a valid configuration stored in
`mailmerge.server.conf` (see below), say:

    mailmerge --no-dry-run --no-limit

## One-step usage

If your SMTP configuration, mailmerge template, data inputs, and configuration
file are all correct, you can make the matches and send the emails in one step
with:

    python run.py --config path/to/config.ini  # NOT RECOMMENDED

**Warning:** If you do it this way, you cannot view the matching results before
sending emails. So, for example, if there are teams who don't get a match, you
will have to handle that separately, and without changing the existing matches,
for which email notifications have already been sent. So I recommend instead
doing this as two separate steps, as described above.

## Email sending configuration

Actually sending the emails requires the proper configuration and
authentication for an SMTP (i.e. mail-sending) server to be stored in the
`mailmerge.server.conf` file.

If the sender is in gmail, one may need to setup 2-step verification and create
an app-specific password for the Google account (a 16-character password); see
[this Google document](https://support.google.com/accounts/answer/185833?hl=en)
for more information. Then, when you are required to enter the password for the
account, enter the app-specific password rather than your account password.

## Collecting the inputs

Each semester, you'll need to do some work to collect the needed information
from mentors and teams.

First, collect from each mentor when they're available. Consider normalizing
the format of the responses, but text is fine and expected; that is, it's not
necessary for a program to understand the various availabilities.

When all mentors have responded, use this data to create the
`mentor_availability.tsv` file. Recall from above that the header row, with
column names, is:
`MentorLetter	MentorFullName	MentorEmailAddress	MentorAvailability`.
You can assign arbitrary letters. Just keep the letter assignment consistent
between this input and the next one.

Then, create a Google Form with two questions. The first question is the team
letter. The second question is a checklist with each mentor's time slot (as
text) as the options, in the form: `<MentorLetter>: <MentorAvailabilityText>`.

When all students have responded, you can download a TSV of the responses,
which should be in the appropriate form for your `team_availability.tsv` file.

Finally, manually create your `team_info.tsv` file with team letters and email
addresses, perhaps from a grading spreadsheet that you've already created.

## Error cases

In any of these cases, no output should be printed apart from a clear error
message.

1. missing input file: which file(s) is missing?
2. invalid input format: which file had an invalid format, and which
   expectation was violated (e.g. "I expected to find 3 tab-separated values on
   the lines of this file.")
3. unmatched mentor letter (i.e. the team input includes a letter for a mentor
   that the mentor availability does not include): which mentor letter could
   not be found? (To trigger, change `A` to `AA` in sample
   `data/mentor_availability.tsv` file.)
4. no student emails found for a team letter: which team letter?
5. no team availability listed for a team letter found in the `team_info.tsv`
   file: which team letter?
6. no solution exists: which team and/or mentor did we fail to find a match
   for? Maybe, what were the matches we found before then?

## TODO

- Support multiple mentors for a single slot (e.g. the Friday class period).
- Halt the program before making matches in the third error case above.
- Document the `show_*` configuration parameters.
- Describe the visual output (see `visual_alignment` above).
- Transition config-file approach to CLI options.
- Add automated tests.

## Acknowledgements

Yiyuan Li wrote all of the initial code for the app. The first commit in this
repo contains almost exactly what he created. So far, 100% of the code is his
creation. Thanks, Yiyuan!
