import os
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str, default="config/example.ini", help='path to the config file')
    args = parser.parse_args()
    os.system("python main.py --config {}".format(args.config))
    os.system("mailmerge --no-limit > mailmerge.out.txt")
