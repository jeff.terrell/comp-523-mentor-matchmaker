import numpy as np
import matplotlib.pyplot as plt


def visual_alignment(team2index, slot2index, team2mentor, slot_codebook, slot_match_matrix, team_slot_match, team_slot_alignment, save_path='None'):
    show_slots_real_name = False
    show_matching_slot = False
    show_mentor_full_name = False

    team_names = list(team2index.keys())
    slot_codes = list(slot2index.keys())
    slot_names = [slot_codebook.get(x, 'TBD') for x in slot_codes] if show_slots_real_name else slot_codes

    # slot_match_matrix_rotate = np.rot90(slot_match_matrix, k=3, axes=(0, 1))

    fig, ax = plt.subplots(figsize=(16,12))
    ax.matshow(slot_match_matrix, cmap='plasma')
    plt.yticks(np.arange(len(team_names)), team_names)
    plt.xticks(np.arange(len(slot_names)), slot_names)
    # plt.xticks(np.arange(len(slot_names)), slot_names, rotation=90)

    # annotate mentor name
#     for team, slots_match in team_slot_match.items():
#         mentor = team2mentor[slots_match]
#         if show_matching_slot:
#             print('Team', team, mentor, slots_match)
            
    for t, s in team_slot_alignment:
        mentor = team2mentor[s] if show_mentor_full_name else team2mentor[s].split()[0]
        ax.text(slot2index[s] - 0.25, team2index[t], mentor,
                bbox=dict(boxstyle='round', facecolor='white', edgecolor='0.3'))


    plt.xlabel('Time Slot')
    plt.ylabel('Team')

    plt.tight_layout()
    plt.show()

    if save_path != 'None':
        plt.savefig(save_path)